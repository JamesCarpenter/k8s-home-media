# k8s-home-media

Goal: A stateless, platform agnostic home media server setup in k8s, with an NFS storage backend.

Born out of a need to do more with less hardware at home, and the desire for quick replacement of the whole setup in the event of a hardware failure.

### Deployment procedure

* Deploy a kubernetes cluster on the platform/hardware of your choice (I use kubespray on a clean CentOS VM)
* Add the relevant usernames/passwords/addresses to the templates
* kubectl apply -f k8s-home-media/

### Features

* NGINX ingress with automatically provisioned letsencrypt certs
* In cluster load balancing by metallb
* All the apps you know and love for your home media server...

### To do

* Deal with secrets more intelligently using an appropriate secret manager